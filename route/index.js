const route = require('express').Router()
const {History,Biodata,controller,userController} = require('../controller')

// HomePage
route.get('/',controller.HomePage)
route.get("/loginPage", controller.getLoginPage)
route.post('/login',controller.loginCheck)

// CP
route.get('/controlpanel',controller.controlPanel)
// route.get('/editpanel',controller.editPanel)
route.get('/editpanel/:id',controller.editPanel)
route.get('/deletepanel/:id',controller.deletePanel)

// addusers
route.post("/addusers", userController.createUsers)
route.get("/addusers", userController.getUsers)

// // edit
route.post('/editusers/:id',userController.editUsers)

// biodata
route.get('/biodata/:id',Biodata.getBio)
route.post('/biodata/:id',Biodata.addBio)

// history
route.get('/history/:id',History.getHis)
route.post('/history/:id',History.addHis)

route.get('*', function(req,res) {
   res.send(`<h1 style='text-align: center;color:red'>WRONG PAGE</h1>`)
})

module.exports = route