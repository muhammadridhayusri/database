const {user_gamebiodata} = require('../models')

class Biodata {
   static async getBio(req,res,next) {
   try{
      const data = await user_gamebiodata.findAll()
      res.json(data)
   } catch(err) {
      console.log(err)
   }
   }

   static async addBio(req,res,next) {
      try{
         const {biodata, userId} = req.body
         const data = await user_gamebiodata.create({biodata, userId})
         res.status(200).json(data)
      } catch(err) {
         console.log(err)
      }
   }
}

module.exports = Biodata