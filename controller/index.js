const History = require('./historycontroller')
const Biodata = require('./biodatacontroller')
const controller = require('./routecontroller')
const userController = require('./usercontroller')

module.exports = ({
   History,Biodata,controller,userController
})