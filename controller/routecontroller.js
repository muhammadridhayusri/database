const {user_game} = require('../models')
const {user_game_biodata} = require('../models')
const {user_game_history} = require('../models')
const userController = require('./usercontroller')

class controller {

   static HomePage(req,res,next) {
      try {
         console.log('Home Page Open')
         res.redirect('/loginPage')
      } catch (err) {
         console.log(err)
      }
   }
   static getLoginPage(req,res,next) {
      try {
         console.log('Login Page Open')
         res.render('login')
      } catch(err) {
         console.log(err)
      }
   }
   static async controlPanel(req,res,next) {
      try {
         
         const data = await user_game.findAll()
         // console.log(data)
         res.render('controlpanel', { data })
      }catch(err) {
         console.log(err)
      }
   }

   static async editPanel(req,res,next) {
      try {
         const id = req.params.id
         console.log('Edit Panel Open')
         const data = await user_game.findAll()
         // const _id = data.find(_id => _id = id )
         // console.log(_id.id)
         res.render('editpanel',{data, id})
      } catch(err) {
         console.log(err)
      }
   }

   static async deletePanel(req,res,next) {
      try {
         const id = req.params.id
         const data = await user_game.destroy({
            where: {
               id
            }
         })
         if (data) {
            res.status(200).redirect('/controlpanel')
         } else {
            res.status(400).send('Data Delete Fail')
         }
      } catch(err) {
         console.log(err)
      }
   }

   static async loginCheck(req,res,next) {
      try {
         let users = await user_game.findAll()
         
         const {name, password} = req.body
         const _userName = users.find(_users => _users.userName === name)
         if (!_userName) {
            res.status(400).send('userName salah')
         } 
         
         if (_userName.userName == name && _userName.passWord == password) {
            res.redirect('/controlpanel')
         } else {
            res.status(400).send('username salah atau password salah')
            console.log('username salah atau password salah')
         }
         // const _userName = users.find(users => users.userName = name)
      } catch(err) {
         console.log(err)
      }
   }
}

module.exports = controller