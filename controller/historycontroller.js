const {user_gamehistory} = require('../models')

class History {
   static async getHis(req,res,next) {
   try{
      const id = req.params.id
      const {data} = await user_gamehistory.findAll()
      res.json({data})
   } catch(err) {
      console.log(err)
   }
   }

   static async addHis(req,res,next) {
      try{
         const {history} = req.body
         const data = await user_gamehistory.create({history})
         res.status(200).json(data)
      } catch(err) {
         console.log(err)
      }
   }
}

module.exports = History