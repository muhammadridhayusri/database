const {user_game} = require('../models')

class userController {
   static async createUsers(req,res,next) {
      try {
         const {userName, passWord} = req.body
         const data = await user_game.create({userName, passWord})
         res.status(200).redirect('/controlpanel')
      }
      catch(err) {
         console.log('Error' + err)
      }
   }
   static async getUsers(req,res,next) {
      try {
         const data = await user_game.findAll()
         res.status(200).render('addusers')
      }
      catch(err) {
         console.log('Error' + err)
      }
   }

   static async editUsers(req,res,next) {
      try {
         const {userName, passWord} = req.body
         const id = req.params.id
         console.log(id)
         // const id = Id + 2
         const data = await user_game.update({
            userName,
            passWord
         },
         {
            where: {
               id
            },
            returning:true
         })
         res.redirect('/controlpanel')
      } catch(err) {
         console.log(err)
      }
   }
}

module.exports = userController