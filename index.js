const express = require('express')
const { sequelize } = require('./models/index.js')
const app = express()
const route = require('./route')

app.use(express.static('views/assets'))
app.set('view engine','ejs')

app.use(express.urlencoded({ extended: true}))
app.use(express.json())

app.use(route)

sequelize.authenticate()
   .then(() => console.log('DB CONNECTED'))
   .catch(err => console.log('Error' + err))

app.listen(5000,() =>{
   console.log('Port Listening on 5000')
})