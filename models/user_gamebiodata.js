'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_gamebiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     static associate(models) {
      user_gamebiodata.belongsTo(models.user_game, {foreignKey: 'userId'})
    }
  }
  user_gamebiodata.init({
    biodata: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_gamebiodata',
  });
  return user_gamebiodata;
};