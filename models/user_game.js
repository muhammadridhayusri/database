'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    static associate(models) {
      user_game.hasOne(models.user_gamehistory)
      user_game.hasMany(models.user_gamebiodata)
    }
  }
  user_game.init({
    userName: DataTypes.STRING,
    passWord: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};