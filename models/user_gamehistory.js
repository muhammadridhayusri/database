'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_gamehistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     static associate(models) {
      user_gamehistory.belongsTo(models.user_game, {foreignKey: 'userId'})
    }
  }
  user_gamehistory.init({
    history: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_gamehistory',
  });
  return user_gamehistory;
};